<?php

namespace Alxw\SortableBehaviorBundle\Twig;

use Alxw\SortableBehaviorBundle\Services\AbstractPositionHandler;
use Twig\Extension\AbstractExtension;

class ObjectPositionExtension extends AbstractExtension
{
    const NAME = 'sortableObjectPosition';

    protected $positionHandler;

    public function __construct(AbstractPositionHandler $positionHandler)
    {
        $this->positionHandler = $positionHandler;
    }

    public function getFunctions(): array
    {
        return [
            new \Twig_SimpleFunction('currentObjectPosition', [$this, 'currentPosition']),
            new \Twig_SimpleFunction('lastPosition', [$this, 'lastPosition'])
        ];
    }

    public function getName(): string
    {
        return self::NAME;
    }

    protected function currentPosition($object): int
    {
        return $this->positionHandler->getCurrentPosition($object);
    }

    protected function lastPosition($object): int
    {
        return $this->positionHandler->getLastPosition($object);
    }
}
