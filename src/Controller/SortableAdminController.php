<?php
/*
 * This file is part of the AlxwSortableBehaviorBundle.
 *
 * (c) Alexis Wurth <awurth.dev@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Alxw\SortableBehaviorBundle\Controller;

use Alxw\SortableBehaviorBundle\Services\AbstractPositionHandler;
use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PropertyAccess\PropertyAccess;

class SortableAdminController extends CRUDController
{
    /**
     * @var AbstractPositionHandler
     */
    protected $positionHandler;

    public function __construct(AbstractPositionHandler $positionHandler)
    {
        $this->positionHandler = $positionHandler;
    }

    public function moveAction(string $position): Response
    {
        $translator = $this->get('translator');

        if (!$this->admin->isGranted('EDIT')) {
            $this->addFlash('sonata_flash_error', $translator->trans('flash_error_no_rights_update_position'));

            return new RedirectResponse($this->admin->generateUrl(
                'list',
                ['filter' => $this->admin->getFilterParameters()]
            ));
        }

        $object = $this->admin->getSubject();

        $lastPositionNumber = $this->positionHandler->getLastPosition($object);
        $newPositionNumber = $this->positionHandler->getPosition($object, $position, $lastPositionNumber);

        PropertyAccess::createPropertyAccessor()
            ->setValue($object, $this->positionHandler->getPositionField($object), $newPositionNumber);

        $this->admin->update($object);

        if ($this->isXmlHttpRequest()) {
            return $this->renderJson([
                'result' => 'ok',
                'objectId' => $this->admin->getNormalizedIdentifier($object)
            ]);
        }

        $this->addFlash('sonata_flash_success', $translator->trans('flash_success_position_updated'));

        return new RedirectResponse($this->admin->generateUrl(
            'list',
            ['filter' => $this->admin->getFilterParameters()]
        ));
    }
}
