<?php
/*
 * This file is part of the AlxwSortableBehaviorBundle.
 *
 * (c) Alexis Wurth <awurth.dev@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Alxw\SortableBehaviorBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Loader\XmlFileLoader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Alias;

class AlxwSortableBehaviorExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container): void
    {
        $configuration = $this->getConfiguration($configs, $container);
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new XmlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.xml');

        $container->setParameter('alxw.sortable.behavior.position.field', $config['position_field']);
        $container->setParameter('alxw.sortable.behavior.sortable_groups', $config['sortable_groups']);

        $positionHandler = sprintf('alxw_sortable_behavior.position.%s', $config['db_driver']);

        $container->setAlias('alxw_sortable_behavior.position', new Alias($positionHandler));
        $container->getAlias('alxw_sortable_behavior.position')->setPublic(true);
    }
}
