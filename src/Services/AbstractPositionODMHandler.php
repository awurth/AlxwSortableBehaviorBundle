<?php
/*
 * This file is part of the AlxwSortableBehaviorBundle.
 *
 * (c) Alexis Wurth <awurth.dev@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Alxw\SortableBehaviorBundle\Services;

use Doctrine\ODM\MongoDB\DocumentManager;

class AbstractPositionODMHandler extends AbstractPositionHandler
{
    /**
     * @var DocumentManager
     */
    protected $documentManager;

    public function __construct(DocumentManager $documentManager)
    {
        $this->documentManager = $documentManager;
    }

    public function getLastPosition($document): int
    {
        $documentClass = $this->getClass($document);

        $parentDocumentClass = true;
        while ($parentDocumentClass) {
            if ($parentDocumentClass = get_parent_class($documentClass)) {
                if ((new \ReflectionClass($parentDocumentClass))->isAbstract()) {
                    break;
                }
                $documentClass = $parentDocumentClass;
            }
        }

        $positionFields = $this->getPositionField($documentClass);
        $result = $this->documentManager
            ->createQueryBuilder($documentClass)
            ->hydrate(false)
            ->select($positionFields)
            ->sort($positionFields, 'desc')
            ->limit(1)
            ->getQuery()
            ->getSingleResult();

        if (is_array($result) && isset($result[$positionFields])) {
            return $result[$positionFields];
        }

        return 0;
    }

    protected function getClass($object): string
    {
        return $this->documentManager->getClassMetadata(get_class($object))->getName();
    }
}
