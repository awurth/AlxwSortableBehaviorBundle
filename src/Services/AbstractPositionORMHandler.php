<?php

/*
 * This file is part of the AlxwSortableBehaviorBundle.
 *
 * (c) Alexis Wurth <awurth.dev@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Alxw\SortableBehaviorBundle\Services;

use Doctrine\ORM\EntityManagerInterface;

class AbstractPositionORMHandler extends AbstractPositionHandler
{
    /**
     * @var array
     */
    private static $cacheLastPosition = [];

    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function getLastPosition($entity): int
    {
        $entityClass = $this->getClass($entity);

        $parentEntityClass = true;
        while ($parentEntityClass) {
            if ($parentEntityClass = get_parent_class($entityClass)) {
                if ((new \ReflectionClass($parentEntityClass))->isAbstract()) {
                    break;
                }
                $entityClass = $parentEntityClass;
            }
        }

        $groups = $this->getSortableGroupsField($entityClass);

        $cacheKey = $this->getCacheKeyForLastPosition($entity, $groups);
        if (!isset(self::$cacheLastPosition[$cacheKey])) {
            $qb = $this->entityManager->createQueryBuilder()
                ->select(sprintf('MAX(t.%s) as last_position', $this->getPositionField($entityClass)))
                ->from($entityClass, 't');

            if ($groups) {
                $i = 1;
                foreach ($groups as $groupName) {
                    $getter = 'get'.$groupName;

                    if ($entity->$getter()) {
                        $qb
                            ->andWhere(sprintf('t.%s = :group_%s', $groupName, $i))
                            ->setParameter(sprintf('group_%s', $i), $entity->$getter());
                        $i++;
                    }
                }
            }

            self::$cacheLastPosition[$cacheKey] = (int)$qb->getQuery()->getSingleScalarResult();
        }

        return self::$cacheLastPosition[$cacheKey];
    }

    protected function getClass($object): string
    {
        return $this->entityManager->getClassMetadata(get_class($object))->getName();
    }

    private function getCacheKeyForLastPosition($entity, array $groups): string
    {
        $cacheKey = $this->getClass($entity);

        foreach ($groups as $groupName) {
            $getter = 'get'.$groupName;

            if ($entity->$getter()) {
                $cacheKey .= '_'.$entity->$getter()->getId();
            }
        }

        return $cacheKey;
    }
}
