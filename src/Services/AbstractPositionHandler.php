<?php
/*
 * This file is part of the AlxwSortableBehaviorBundle.
 *
 * (c) Alexis Wurth <awurth.dev@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Alxw\SortableBehaviorBundle\Services;

use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessor;

abstract class AbstractPositionHandler
{
    /**
     * From config
     *
     * @var array
     */
    protected $positionField;

    /**
     * From config
     *
     * @var array
     */
    private $sortableGroups;

    /**
     * @var PropertyAccessor
     */
    private $accessor;

    abstract public function getLastPosition($object): int;

    abstract protected function getClass($object): string;

    public function setPositionField(array $positionField): void
    {
        $this->positionField = $positionField;
    }

    public function setSortableGroups(array $sortableGroups): void
    {
        $this->sortableGroups = $sortableGroups;
    }

    public function getPositionField($object): string
    {
        if (is_object($object)) {
            $object = $this->getClass($object);
        }

        return $this->positionField['objects'][$object] ?? $this->positionField['default'];
    }

    public function getSortableGroupsField($object): array
    {
        if (is_object($object)) {
            $object = $this->getClass($object);
        }

        return $this->sortableGroups['objects'][$object] ?? [];
    }

    /**
     * @param object $object
     *
     * @return int
     */
    public function getCurrentPosition($object)
    {
        return $this->getAccessor()->getValue($object, $this->getPositionField($object));
    }

    public function getPosition($object, string $movePosition, int $lastPosition): int
    {
        $currentPosition = $this->getCurrentPosition($object);
        $newPosition = 0;

        switch ($movePosition) {
            case 'up':
                if ($currentPosition > 0) {
                    $newPosition = $currentPosition - 1;
                }
                break;

            case 'down':
                if ($currentPosition < $lastPosition) {
                    $newPosition = $currentPosition + 1;
                }
                break;

            case 'top':
                if ($currentPosition > 0) {
                    $newPosition = 0;
                }
                break;

            case 'bottom':
                if ($currentPosition < $lastPosition) {
                    $newPosition = $lastPosition;
                }
                break;

            default:
                if (is_numeric($movePosition)) {
                    $newPosition = (int)$movePosition;
                }

        }

        return $newPosition;
    }

    private function getAccessor(): PropertyAccessor
    {
        if (!$this->accessor) {
            $this->accessor = PropertyAccess::createPropertyAccessor();
        }

        return $this->accessor;
    }
}
