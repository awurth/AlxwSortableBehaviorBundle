<?php
/*
 * This file is part of the AlxwSortableBehaviorBundle.
 *
 * (c) Alexis Wurth <awurth.dev@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Alxw\SortableBehaviorBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class AlxwSortableBehaviorBundle extends Bundle
{
}
